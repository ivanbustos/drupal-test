<?php

namespace Drupal\bowling\attempt;

/**
 * A class used to define an attempt.
 */
class Attempt {

  const MIN_PINS_DOWNED = 0;
  const MAX_PINS_DOWNED = 10;

  /**
   * The number of pins downed in this attempt.
   *
   * @var int
   */
  private int $pinsDowned;

  /**
   * Initializes an attempt.
   *
   * @param int $pins_downed
   *   The number of pins downed in this attempt.
   *
   * @throws \Exception
   *   May throw an exception if attempting to down more or less pins than
   *   the set constraints.
   */
  public function __construct(int $pins_downed) {
    if ($pins_downed < self::MIN_PINS_DOWNED || $pins_downed > self::MAX_PINS_DOWNED) {
      throw new \Exception('Number of pins downed is out of range.');
    }
    $this->pinsDowned = $pins_downed;
  }

  /**
   * Property mutator.
   *
   * @return int
   *   Returns the value of the $pinsDowned class property.
   */
  public function getPinsDowned(): int {
    return $this->pinsDowned;
  }

}
