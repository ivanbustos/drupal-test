<?php

namespace Drupal\bowling\attempt;

/**
 * Handles a collection of attempts.
 */
class AttemptList implements \Countable, \Iterator {

  const MAX_ALLOWED_ATTEMPTS = 2;
  const MAX_ALLOWED_PINS_DOWNED = 10;

  /**
   * The inner set of attempts.
   *
   * @var \Drupal\bowling\attempt\Attempt[]
   */
  private array $attempts = [];

  /**
   * The amount of pins downed in all attempts.
   *
   * @var int
   */
  private int $pinsDowned = 0;

  /**
   * The current set index.
   *
   * @var int
   */
  private int $currentIndex = 0;

  /**
   * Adds an attempt to the collection.
   *
   * @param \Drupal\bowling\attempt\Attempt $attempt
   *   The attempt to add.
   *
   * @throws \Exception
   *   An exception can be thrown if list of attempts exceeds the max allowed.
   *   Another exception can happen if the amount of pins downed somehow exceeds
   *   the amount of maximum downed pins.
   */
  public function addAttempt(Attempt $attempt) {
    if (count($this->attempts) === self::MAX_ALLOWED_ATTEMPTS) {
      throw new \Exception('An attempt list can only have up to two attempts.');
    }
    $this->pinsDowned += $attempt->getPinsDowned();
    $this->attempts[] = $attempt;
    if ($this->pinsDowned > self::MAX_ALLOWED_PINS_DOWNED) {
      throw new \Exception(sprintf('The sum of all pins downed during attempts cannot possibly be greater than %d', self::MAX_ALLOWED_PINS_DOWNED));
    }
  }

  /**
   * Property accessor.
   *
   * @return int
   *   The value of the $pinsDowned class property.
   */
  public function getPinsDowned(): int {
    return $this->pinsDowned;
  }

  /**
   * {@inheritDoc}
   */
  public function current(): Attempt {
    return $this->attempts[$this->currentIndex];
  }

  /**
   * {@inheritDoc}
   */
  public function next(): void {
    $this->currentIndex++;
  }

  /**
   * {@inheritDoc}
   */
  public function key(): int {
    return $this->currentIndex;
  }

  /**
   * {@inheritDoc}
   */
  public function valid(): bool {
    return isset($this->attempts[$this->currentIndex]);
  }

  /**
   * {@inheritDoc}
   */
  public function rewind(): void {
    $this->currentIndex = 0;
  }

  /**
   * {@inheritDoc}
   */
  public function count(): int {
    return count($this->attempts);
  }

}
