<?php

namespace Drupal\bowling;

/**
 * The contract for any game program.
 */
interface GameServiceInterface {

  /**
   * Creates a new game for a new user.
   *
   * @param string $name
   *   The player name.
   *
   * @return \Drupal\bowling\Game
   *   The already initialized game.
   */
  public function createGameFor(string $name): Game;

}
