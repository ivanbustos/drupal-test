<?php

namespace Drupal\bowling;

use Drupal\bowling\turn\SpareTurn;
use Drupal\bowling\turn\StrikeTurn;
use Drupal\bowling\turn\TurnInterface;
use Drupal\bowling\turn\TurnList;

/**
 * Main class for the bowling game.
 */
class Game {

  const MAX_ALLOWED_BONUS_TURNS = 2;
  const MAX_ALLOWED_TURNS = 10;

  /**
   * The player name.
   *
   * @var string
   */
  private string $playerName;

  /**
   * The amount of turns so far in this game.
   *
   * @var \Drupal\bowling\turn\TurnList
   */
  private TurnList $turns;

  /**
   * Whether this game is eligible to have any additional bonus turns.
   *
   * @return bool
   *   Whether this game is eligible to have any additional bonus turns.
   */
  public function isEligibleForAdditionalBonusTurn(): bool {
    if ($this->turns->count() >= (self::MAX_ALLOWED_TURNS + self::MAX_ALLOWED_BONUS_TURNS)) {
      return FALSE;
    }
    $this->turns->unwind();
    if ($this->turns->count() === (self::MAX_ALLOWED_TURNS + self::MAX_ALLOWED_BONUS_TURNS - 1)) {
      // Only allow a consecutive bonus round if last bonus was a strike.
      return get_class($this->turns->current()) === StrikeTurn::class;
    }
    return $this->turns->count() === self::MAX_ALLOWED_TURNS && get_class($this->turns->current()) === StrikeTurn::class;
  }

  /**
   * Appends additional bonus turns if eligible.
   *
   * @param \Drupal\bowling\turn\TurnInterface $turn
   *   The turn being added.
   *
   * @throws \Exception
   *   Thrown if attempting to add a bonus turn if not eligible.
   */
  public function addBonusTurn(TurnInterface $turn): void {
    if (!$this->isEligibleForAdditionalBonusTurn()) {
      throw new \Exception('Unable to add more bonus turns to this game');
    }
    $this->turns->addTurn($turn, TRUE);
  }

  /**
   * Initializes a new game.
   *
   * @param string $player_name
   *   The player name.
   */
  public function __construct(string $player_name) {
    $this->playerName = $player_name;
    $this->turns = new TurnList(self::MAX_ALLOWED_TURNS);
  }

  /**
   * Property accessor.
   *
   * @return string
   *   The value of the $playerName class property.
   */
  public function getPlayerName(): string {
    return $this->playerName;
  }

  /**
   * Property accessor.
   *
   * @return \Drupal\bowling\turn\TurnList
   *   The value of the turns class property.
   */
  public function getTurnList(): TurnList {
    return $this->turns;
  }

  /**
   * Calculates the score of this game so far.
   *
   * @return int
   *   The score of the game so far.
   */
  public function getScore(): int {
    $score = 0;
    foreach ($this->turns as $turn) {
      // If current turn is a spare, add the first throw in next turn.
      if (get_class($turn) === SpareTurn::class) {
        $next_turn = $this->turns->offsetGet($this->turns->key() + 1);
        if (is_object($next_turn)) {
          $next_turn->getAttempts()->rewind();
          $score += $next_turn->getAttempts()->current()->getPinsDowned();
        }
      }

      // If current turn is a strike, add the following two attempts.
      if (get_class($turn) === StrikeTurn::class) {
        // This only applies if this is not the last turn.
        if ($this->turns->key() + 1 <= self::MAX_ALLOWED_TURNS - 1) {
          $next_turn = $this->turns->offsetGet($this->turns->key() + 1);
          if (is_object($next_turn)) {
            if (get_class($next_turn) === StrikeTurn::class) {
              // Add the score of the first attempt in both the two next turns.
              $next_turn->getAttempts()->rewind();
              $score += $next_turn->getAttempts()->current()->getPinsDowned();
              $next_turn = $this->turns->offsetGet($this->turns->key() + 2);
              if (is_object($next_turn)) {
                $next_turn->getAttempts()->rewind();
                $score += $next_turn->getAttempts()->current()->getPinsDowned();
              }
            }
            else {
              // Add the score of both attempts.
              $score += $next_turn->getScore();
            }
          }
        }
      }

      // Add the corresponding score of this turn.
      $score += $turn->getScore();
    }

    return $score;
  }

}
