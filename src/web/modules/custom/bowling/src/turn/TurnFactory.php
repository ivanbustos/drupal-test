<?php

namespace Drupal\bowling\turn;

use Drupal\bowling\attempt\AttemptList;

/**
 * A factory used to create new turns.
 */
final class TurnFactory {

  const MAX_ALLOWED_PINS_DOWNED = 10;

  /**
   * Creates a turn from an attempt list.
   *
   * @param \Drupal\bowling\attempt\AttemptList $attempts
   *   The attempt list used to create the turn.
   *
   * @return \Drupal\bowling\turn\TurnInterface
   *   The new turn.
   */
  public static function create(AttemptList $attempts): TurnInterface {
    if ($attempts->getPinsDowned() !== TurnFactory::MAX_ALLOWED_PINS_DOWNED) {
      return new DefaultTurn($attempts);
    }
    if ($attempts->count() === 1) {
      return new StrikeTurn($attempts);
    }
    return new SpareTurn($attempts);
  }

}
