<?php

namespace Drupal\bowling\turn;

/**
 * A class intended to identify lucky strike turns.
 */
class StrikeTurn extends Turn {

  /**
   * {@inheritDoc}
   */
  public function getScore(): int {
    return 10;
  }

}
