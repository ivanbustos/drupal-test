<?php

namespace Drupal\bowling\turn;

/**
 * The collection of turns.
 */
class TurnList implements \Countable, \Iterator {

  /**
   * The inner set of turns.
   *
   * @var \Drupal\bowling\turn\TurnInterface[]
   */
  private array $turns = [];


  /**
   * The current set index.
   *
   * @var int
   */
  private int $currentIndex = 0;

  /**
   * The maximum number of turns allowed.
   *
   * @var int
   */
  private int $maxAllowedTurns;

  /**
   * Initializes a new TurnList object.
   *
   * @param int $max_allowed_turns
   *   The maximum turns this turn collection can contain.
   */
  public function __construct(int $max_allowed_turns) {
    $this->maxAllowedTurns = $max_allowed_turns;
  }

  /**
   * Adds a turn to the collection.
   *
   * @param \Drupal\bowling\turn\TurnInterface $turn
   *   The turn to add.
   * @param bool $force
   *   Whether to force insertion even if the turn list has reached its limit.
   *
   * @throws \Exception
   *   An exception can be thrown if list of turns exceeds the max allowed.
   */
  public function addTurn(TurnInterface $turn, $force = FALSE) {
    if (count($this->turns) >= $this->maxAllowedTurns && !$force) {
      throw new \Exception('Unable to add more turns to this turn list');
    }
    $this->turns[] = $turn;
  }

  /**
   * {@inheritDoc}
   */
  public function current(): Turn {
    return $this->turns[$this->currentIndex];
  }

  /**
   * Extracts a turn given its index.
   *
   * @param int $index
   *   The index to search for.
   *
   * @return \Drupal\bowling\turn\TurnInterface|false
   *   The turn or false if the turn is not found.
   */
  public function offsetGet(int $index): Turn|false {
    if (!isset($this->turns[$index])) {
      return FALSE;
    }
    return $this->turns[$index];
  }

  /**
   * {@inheritDoc}
   */
  public function prev(): void {
    $this->currentIndex--;
  }

  /**
   * {@inheritDoc}
   */
  public function next(): void {
    $this->currentIndex++;
  }

  /**
   * {@inheritDoc}
   */
  public function key(): int {
    return $this->currentIndex;
  }

  /**
   * {@inheritDoc}
   */
  public function valid(): bool {
    return isset($this->turns[$this->currentIndex]);
  }

  /**
   * {@inheritDoc}
   */
  public function rewind(): void {
    $this->currentIndex = 0;
  }

  /**
   * Moves the index to the end of the collection.
   */
  public function unwind(): void {
    $this->currentIndex = $this->count() - 1;
  }

  /**
   * {@inheritDoc}
   */
  public function count(): int {
    return count($this->turns);
  }

}
