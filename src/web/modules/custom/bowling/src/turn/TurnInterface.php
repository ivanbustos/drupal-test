<?php

namespace Drupal\bowling\turn;

use Drupal\bowling\attempt\AttemptList;

/**
 * The minimum contract for all turns.
 */
interface TurnInterface {

  /**
   * Returns the list of attempts in a given turn.
   *
   * @return \Drupal\bowling\attempt\AttemptList
   *   The list of attempts in a turn.
   */
  public function getAttempts(): AttemptList;

  /**
   * The current turn score.
   *
   * @return int
   *   The current turn score.
   */
  public function getScore(): int;

}
