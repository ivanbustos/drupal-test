<?php

namespace Drupal\bowling\turn;

use Drupal\bowling\attempt\Attempt;
use Drupal\bowling\attempt\AttemptList;

/**
 * The base class for turns.
 */
abstract class Turn implements TurnInterface {

  /**
   * The collection of attempts.
   *
   * @var \Drupal\bowling\attempt\AttemptList
   */
  private AttemptList $attempts;

  /**
   * Initializes the turn.
   *
   * @param \Drupal\bowling\attempt\AttemptList $attempts
   *   The list of attempts that comprise this turn.
   */
  public function __construct(AttemptList $attempts) {
    $this->attempts = $attempts;
  }

  /**
   * Property accessor.
   *
   * @return \Drupal\bowling\attempt\AttemptList[]
   *   The value of the $attempts class property.
   */
  public function getAttempts(): AttemptList {
    return $this->attempts;
  }

  /**
   * Calculates the score of this turn.
   *
   * @return int
   *   The score of this turn.
   */
  public function getScore(): int {
    $result = array_reduce(iterator_to_array($this->attempts), function (int $carry, Attempt $item) {
      return $carry + $item->getPinsDowned();
    }, 0);

    // Rewind the attempt list.
    $this->attempts->rewind();

    return $result;
  }

}
