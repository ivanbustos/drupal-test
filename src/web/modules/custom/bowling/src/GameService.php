<?php

namespace Drupal\bowling;

/**
 * A Drupal service used to initialize bowling games.
 */
class GameService implements GameServiceInterface {

  /**
   * Initializes a new game for a given user.
   *
   * @param string $name
   *   The player name.
   *
   * @return Game
   *   The initialized new game.
   */
  public function createGameFor(string $name): Game {
    return new Game($name);
  }

}
