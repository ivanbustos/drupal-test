<?php

namespace Drupal\bowling\drush;

use Drupal\bowling\attempt\Attempt;
use Drupal\bowling\attempt\AttemptList;
use Drupal\bowling\Game;
use Drupal\bowling\GameServiceInterface;
use Drupal\bowling\turn\TurnFactory;
use Drush\Commands\DrushCommands;

/**
 * The drush command definition.
 */
class GameCommand extends DrushCommands {

  /**
   * The game service.
   *
   * @var \Drupal\bowling\GameServiceInterface
   */
  protected GameServiceInterface $gameService;

  /**
   * Initializes the GameCommand object.
   *
   * @param \Drupal\bowling\GameServiceInterface $game_service
   *   An instance of the game service.
   */
  public function __construct(GameServiceInterface $game_service) {
    $this->gameService = $game_service;
    parent::__construct();
  }

  /**
   * The Drush command used to play the bowling game.
   *
   * @command bowling
   *
   * @throws \Exception
   *   May throw an exception during the game if using parameters outside of
   *   given constraints.
   */
  public function bowling(): void {
    $this->output->writeln('<<< Welcome to Bowling! >>>');

    // Get player count.
    $player_count = (int) ($this->io()->ask('How many players?', 1));

    if ($player_count <= 0) {
      throw new \Exception('Game exited with no players.');
    }

    // Get player names.
    $players = [];
    for ($i = 1; $i <= $player_count; $i++) {
      $players[] = $this->io()->ask(sprintf('Enter the name for player %d', $i));
    }

    // Begin games.
    /** @var \Drupal\bowling\Game[] $games */
    $games = [];
    foreach ($players as $player) {
      $games[] = $this->gameService->createGameFor($player);
    }
    unset($players);

    // Start game.
    for ($i = 1; $i <= Game::MAX_ALLOWED_TURNS; $i++) {
      $this->output->writeln(sprintf('<<< TURN %d >>>', $i));
      foreach ($games as $game) {
        $this->output->writeln(sprintf('Your turn %s', $game->getPlayerName()));
        $attempts_list = new AttemptList();
        $attempts_list->addAttempt(new Attempt((int) $this->io()->ask('How many pins did you knock down in first throw? [number]')));
        if ($attempts_list->getPinsDowned() < Attempt::MAX_PINS_DOWNED) {
          $attempts_list->addAttempt(new Attempt((int) $this->io()->ask('How many pins did you knock down in second throw? [number]')));
        }
        $turn = TurnFactory::create($attempts_list);
        $game->getTurnList()->addTurn($turn);
      }
    }

    // Bonus turns.
    foreach ($games as $game) {
      while ($game->isEligibleForAdditionalBonusTurn()) {
        $this->output->writeln(sprintf('<<<< %s! You have a bonus turn! >>>', $game->getPlayerName()));
        $attempts_list = new AttemptList();
        $attempts_list->addAttempt(new Attempt((int) $this->io()->ask('How many pins did you knock down in first throw? [number]')));
        if ($attempts_list->getPinsDowned() < Attempt::MAX_PINS_DOWNED) {
          $attempts_list->addAttempt(new Attempt((int) $this->io()->ask('How many pins did you knock down in second throw? [number]')));
        }
        $turn = TurnFactory::create($attempts_list);
        $game->addBonusTurn($turn);
      }
    }

    // Display results.
    $this->output->writeln('<<<< Game Finished! >>>>');
    $max_score = 0;
    $max_index = 0;
    foreach ($games as $index => $game) {
      if ($game->getScore() > $max_score) {
        $max_score = $game->getScore();
        $max_index = $index;
      }
      $this->output->writeln(sprintf('%s, you got %d points!', $game->getPlayerName(), $game->getScore()));
    }

    // Display winner.
    $this->output->writeln(sprintf('%s, you are the WINNER by scoring %d points!', $games[$max_index]->getPlayerName(), $games[$max_index]->getScore()));

  }

}
