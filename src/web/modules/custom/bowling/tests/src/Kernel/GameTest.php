<?php

namespace Drupal\Tests\bowling\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\bowling\Kernel\utils\GameFactory;

/**
 * Tests the functionality of the bowling game.
 *
 * @group bowling
 */
class GameTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['bowling'];

  /**
   * Tests game scores.
   *
   * @dataProvider dataProvider
   */
  public function testsGameScore(string $game_expression, int $expected_score) {
    $game = GameFactory::createGameFromExpression(static::randomMachineName(), $game_expression);
    static::assertEquals($expected_score, $game->getScore());
  }

  /**
   * Provides data for testsGameScore.
   */
  public function dataProvider(): array {
    $data = [];

    // Zero point game.
    $data[] = [
      '[(0,0), (0,0), (0,0), (0,0), (0,0), (0,0), (0,0), (0,0), (0,0), (0,0)]',
      0,
    ];

    // Five points game.
    $data[] = [
      '[(0,0), (0,0), (5,0), (0,0), (0,0), (0,0), (0,0), (0,0), (0,0), (0,0)]',
      5,
    ];

    // Five points game.
    $data[] = [
      '[(0,0), (0,0), (0,0), (0,0), (0,0), (0,0), (0,5), (0,0), (0,0), (0,0)]',
      5,
    ];

    // A game with a spare.
    $data[] = [
      '[(0,0), (0,0), (0,0), (0,0), (0,0), (0,0), (5,5), (0,0), (0,0), (0,0)]',
      10,
    ];

    // A game with a strike.
    $data[] = [
      '[(0,0), (0,0), (0,0), (0,0), (0,0), (10), (0,0), (0,0), (0,0), (0,0)]',
      10,
    ];

    // A game with a spare where the next attempt hits 2 pins.
    $data[] = [
      '[(0,0), (0,0), (0,0), (5,5), (2,0), (0,0), (0,0), (0,0), (0,0), (0,0)]',
      14,
    ];

    // A game with a strike where the next attempt hits 3 and 4 pins.
    $data[] = [
      '[(0,0), (0,0), (0,0), (10), (2,4), (0,0), (0,0), (0,0), (0,0), (0,0)]',
      22,
    ];

    // Three consecutive strikes.
    $data[] = [
      '[(10), (10), (10), (0,0), (0,0), (0,0), (0,0), (0,0), (0,0), (0,0)]',
      60,
    ];

    // Nine consecutive strikes.
    $data[] = [
      '[(10), (10), (10), (10), (10), (10), (10), (10), (10), (0,0)]',
      240,
    ];

    // Ten consecutive strikes with no bonus turn.
    $data[] = [
      '[(10), (10), (10), (10), (10), (10), (10), (10), (10), (10)]',
      270,
    ];

    // A perfect game ending in an awful bonus turn.
    $data[] = [
      '[(10), (10), (10), (10), (10), (10), (10), (10), (10), (10), (0,0)]',
      270,
    ];

    // A perfect game ending in a poor bonus turn.
    $data[] = [
      '[(10), (10), (10), (10), (10), (10), (10), (10), (10), (10), (1,2)]',
      274,
    ];

    // A perfect game ending in a spare bonus turn.
    $data[] = [
      '[(10), (10), (10), (10), (10), (10), (10), (10), (10), (10), (5,5)]',
      285,
    ];

    // A perfect game ending in a perfect bonus turn.
    $data[] = [
      '[(10), (10), (10), (10), (10), (10), (10), (10), (10), (10), (10), (10)]',
      300,
    ];

    // Another example as documented in requirement.
    $data[] = [
      '[(10), (5,0), (5,0), (0,0), (0,0), (0,0), (0,0), (0,0), (0,0), (0,0)]',
      25,
    ];

    // Another example as documented in requirement.
    $data[] = [
      '[(5,5), (3,2), (0,0), (0,0), (0,0), (0,0), (0,0), (0,0), (0,0), (0,0)]',
      18,
    ];

    return $data;
  }

}
