<?php

namespace Drupal\Tests\bowling\Kernel\utils;

use Drupal\bowling\attempt\Attempt;
use Drupal\bowling\attempt\AttemptList;
use Drupal\bowling\Game;
use Drupal\bowling\turn\TurnFactory;

/**
 * Tool class used to automatically play a bowling game given an expression.
 */
final class GameFactory {

  /**
   * Plays a new Bowling game given a string expression.
   *
   * @param string $player_name
   *   The player name.
   * @param string $expression
   *   The given game expression.
   *
   * @return \Drupal\bowling\Game
   *   The game, already played.
   *
   * @throws \Exception
   *   Thrown if there is an illegal action.
   */
  public static function createGameFromExpression(string $player_name, string $expression): Game {
    $expression = trim($expression);
    if (!preg_match('/\[(?:\(\d{1,2}(?:,\d{1,2})?\),\s)+\(\d{1,2}(?:,\d{1,2})?\)]/', $expression)) {
      throw new \Exception('Expression used does not match correct format.');
    }
    // Remove brackets and first and last parentheses.
    $expression = substr($expression, 2, strlen($expression) - 4);

    // Remove spaces.
    $expression = str_replace(' ', '', $expression);

    // Explode the expression.
    $turns_a = explode('),(', $expression);

    // Create game.
    $game = new Game($player_name);

    // Add turns and attempts.
    foreach ($turns_a as $turn_s) {
      $attempt_list = new AttemptList();
      $pins_down_attempts = explode(',', $turn_s);
      foreach ($pins_down_attempts as $pins_down) {
        $attempt = new Attempt($pins_down);
        $attempt_list->addAttempt($attempt);
      }
      try {
        $turn = TurnFactory::create($attempt_list);
        $game->getTurnList()->addTurn($turn);
      }
      catch (\Exception $e) {
        // This can happen if we are adding a turn beyond the limit.
        $game->addBonusTurn(TurnFactory::create($attempt_list));
      }
    }

    return $game;
  }

}
