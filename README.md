1. Install Drupal dependencies by running:

`docker run --rm --user "$(id -u):$(id -g)" --volume "$(pwd)/src:/app" -it composer:2 composer install`

2. Install Drupal:

`docker-compose up -d && docker-compose run --rm php bash -c 'wait-for -t 180 mariadb:3306 -- echo \"mariadb is available.\"' && docker-compose run --rm php bash -c 'drush site:install --db-url=mysql://root:root@mariadb:3306/site --existing-config -y'`

3. Run the game:

`docker-compose run --rm php drush bowling`

## Running Tests

`docker-compose run --rm php php core/scripts/run-tests.sh --php /usr/local/bin/php --sqlite /tmp/test.sqlite --dburl mysql://root:root@mariadb:3306/site --color bowling`
